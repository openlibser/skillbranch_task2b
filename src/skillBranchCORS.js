module.exports = function allowCrossDomain(req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://account.skill-branch.ru');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
};
