function checkWordValidity(word) {
  const notValidChars = /\d|_|\/|\\|!|@|#|\$|%|\^|&|\*|\(|\)|-/ig;
  return notValidChars.test(word) === false;
}

function validateWords(words) {
  if (words.length === 0) return false;
  if (words.length > 3) return false;
  const isAllWordsValid = words.filter(checkWordValidity).length === words.length;
  if (!isAllWordsValid) return false;
  return true;
}

module.exports = (fullName = '') => {
  const words = fullName.split(' ')
                .map(word => word.trim())
                .filter(word => word.length > 0);
  const isValid = validateWords(words);
  return isValid;
};
