const express = require('express');
const morgan = require('morgan');

const fullNameValidator = require('./fullNameValidator');
const canonizeFIO = require('./canonizeFIO');
const allowCrossDomain = require('./skillBranchCORS');

const app = express();

morgan.token('decodedURL', req => decodeURIComponent(req.url));

app.use(allowCrossDomain);
app.use(morgan(':method :decodedURL :status :res[content-length] - :response-time ms'));

app.get('/', (req, res) => {
  const ERROR_MESSAGE = 'Invalid fullname';
  const isValid = fullNameValidator(req.query.fullname);
  if (isValid) {
    res.send(canonizeFIO(req.query.fullname));
  } else {
    res.send(ERROR_MESSAGE);
  }
});

app.listen(3000, () => console.log('Server runing on 3000 port!'));
