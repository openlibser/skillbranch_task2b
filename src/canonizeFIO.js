function capitalize(str) {
  return str[0].toUpperCase() + str.slice(1).toLowerCase();
}

function makeInitials(name, patronymic) {
  const N = name ? `${name.substr(0, 1).toUpperCase()}.` : '';
  const P = patronymic ? `${patronymic.substr(0, 1).toUpperCase()}.` : '';
  return `${N} ${P}`.trim();
}

function makeResponsetext(words) {
  let result;
  switch (words.length) {
    case 1:
      result = capitalize(words[0]);
      break;
    case 2:
      result = `${capitalize(words[1])} ${makeInitials(words[0])}`;
      break;
    case 3:
      result = `${capitalize(words[2])} ${makeInitials(words[0], words[1])}`;
      break;
    default:
      throw new Error('Invalid words length');
  }

  return result;
}

module.exports = (fullName) => {
  const words = fullName.split(' ')
                .map(word => word.trim())
                .filter(word => word.length > 0);
  return makeResponsetext(words);
};
